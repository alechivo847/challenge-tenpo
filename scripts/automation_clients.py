import csv
import from configparser import ConfigParser

cnf = ConfigParser()
cnf.read('config.ini')

def parser_by_city(city):
    csv_file = cnf.get('client', 'path_csv')
    lClients = []

    with open(csv_file, 'r') as file:
        oRead = csv.DictReader(file)
        for row in oRead:
            if row[cnf.get('client', 'field_filter')] == Ciudad:
                client = f"{row['Nombre']} {row['Apellido']} <{row['Email']}>"
                lClients.append(client)

    return lClients
