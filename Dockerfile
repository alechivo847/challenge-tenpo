FROM golang:1.21.1-alpine3.18

LABEL maintainer="iam.alejandro.martinez847@gmail.com"

RUN apk add --no-cache curl \
  && apk add --no-cache git

ADD . /go/src/gitlab.com/alechivo847/challenge-tenpo
RUN curl -s https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
WORKDIR /go/src/gitlab.com/alechivo847/challenge-tenpo

RUN go mod init \
  && go get golang.org/x/crypto/acme/autocert \
  && go install

ENTRYPOINT /go/bin/challenge-tenpo

# expose port http
EXPOSE 80
