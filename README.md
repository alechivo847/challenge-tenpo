# Challenge-tenpo

===========

In this repository you will find the kubernetes files to expose a web service over https and http. The same is divided by the package manager "Helm" through templates and also in a single yaml to run in any cloud k8s.

Then there is a CICD folder with a pipeline to run tests on "Checkov" on "Terraform" for the different IAC requested.

An iac folder was also versioned with all the .tf of "Terraform" to be able to build a k8s cluster, create a mysql instance, create an Azure storage media, and then .tf of RO and RW user registration.

Finally a scripts folder where you can find an app that operates on a .csv file.

===========

Application on Kubernetes
------------

### The service running on the k8s cluster through a docker image with Golang (in registry publishes in Gitlab), has port 80 and 443 to reach it from the ingress/lb:

```bash
$ kubectl describe svc challenge-tenpo-svc

Name:              challenge-tenpo-svc
Namespace:         qa-system
Labels:            app.kubernetes.io/managed-by=Helm
Annotations:       meta.helm.sh/release-name: challenge-tenpo
                   meta.helm.sh/release-namespace: qa-system
Selector:          app.kubernetes.io/instance=challenge-tenpo,app.kubernetes.io/name=challenge-tenpo-app
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.152.183.90
IPs:               10.152.183.90
Port:              <unset>  80/TCP
TargetPort:        443/TCP
Endpoints:         <none>
Session Affinity:  None
Events:            <none>
```

### We list the pods running which should be a minimum of 2 up to a maximum of 10 (which depends on the cpu consumption and the HPA scales it):

```bash
$ kubectl get horizontalpodautoscalers.autoscaling | grep tenpo

challenge-tenpo-hpa   Deployment/challenge-tenpo   <unknown>/40%   2         10        2          5h35m
```

### When sending a request with a "ping" inside the vpc of the k8s cluster, the application that is internally fighting for the port 80, answers you "pong" if you send something else it should answer you error http 404:
```
$ kubectl exec challenge-tenpo-6589499f9-cvlsw -- curl -s http://localhost/ping
pong
...
