resource "azuread_user" "iam_admin" {
  display_name        = "Admin User"
  mail_nickname       = "adminuser"
  user_principal_name = var.user_principal_name
  password            = var.password
}

resource "azurerm_role_definition" "iam_admin" {
  name = "Contributor"
  scope = "/subscriptions/${var.suscription_id}"
  permissions {
    actions     = ["Microsoft.Authorization/*/*", "Microsoft.Resources/subscriptions/resourceGroups/*", "Microsoft.Storage/*/*", "Microsoft.Kubernetes/connectedClusters/*/*", "Microsoft.DBforMySQL/*/*"]
    not_actions = []
  }
}

resource "azurerm_role_assignment" "iam_admin" {
  scope                = "/subscriptions/${var.suscription_id}"
  role_definition_name = azurerm_role_definition.iam_admin.name
  principal_id         = azuread_user.iam_admin.object_id
}
