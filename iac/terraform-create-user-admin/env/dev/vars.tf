variable "suscription_id" {
 description = "Suscription ID of Azure"
 type        = string
}

variable "user_principal_name" {
 description = "Email user principal"
 type        = string
}

variable "password" {
 description = "Password for the user principal"
 type        = string
}
