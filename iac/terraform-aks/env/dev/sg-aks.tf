data "azuread_group" "admin-team" {
  display_name     = "group_challenge"
}

module "aks" {
  source                           = "Azure/aks/azurerm"
  version                          = "4.14.0"
  resource_group_name              = azurerm_resource_group.sg_aks_rg.name
  kubernetes_version               = "1.26.6"
  orchestrator_version             = "1.26.6"
  prefix                           = "${var.env}-${var.group}-${var.app}"
  cluster_name                     = "${var.env}-${var.group}-${var.app}-cluster"
  vnet_subnet_id                   = module.vnet.vnet_subnets[0]
  network_plugin                   = "azure"
  os_disk_size_gb                  = 50
  sku_tier                         = "Paid"
  enable_role_based_access_control = true
  rbac_aad_admin_group_object_ids  = [data.azuread_group.admin-team.id]
  rbac_aad_managed                 = true
  enable_azure_policy              = true
  enable_auto_scaling              = true
  enable_host_encryption           = true
  agents_min_count                 = 1
  agents_max_count                 = 5
  agents_count                     = null
  agents_max_pods                  = 110
  agents_pool_name                 = "nodepool"
  agents_availability_zones        = ["1", "2", "3"]
  agents_type                      = "VirtualMachineScaleSets"
  agents_size                      = "Standard_DS2_v2"

  agents_labels = {
    "nodepool" : "nodepool"
  }


  agents_tags = {
    "Agent" : "defaultnodepoolagent"
  }

  net_profile_dns_service_ip       = var.net_profile_dns_service_ip
  net_profile_docker_bridge_cidr   = var.net_profile_docker_bridge_cidr
  net_profile_service_cidr         = var.net_profile_service_cidr

  depends_on = [module.vnet]
}
