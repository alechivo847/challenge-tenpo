resource "azuread_user" "iam_reader" {
  display_name        = "Read Only User"
  mail_nickname       = "readonlyuser"
  user_principal_name = var.user_principal_name
  password            = var.password
}

resource "azurerm_role_definition" "iam_reader" {
  name = "Reader"
  scope = "/subscriptions/${var.suscription_id}"
  permissions {
    actions     = ["Microsoft.Authorization/*/read", "Microsoft.Resources/subscriptions/resourceGroups/read", "Microsoft.Storage/*/read", "Microsoft.Kubernetes/connectedClusters/*/read", "Microsoft.DBforMySQL/*/read"]
    not_actions = []
  }
}

resource "azurerm_role_assignment" "iam_reader" {
  scope                = "/subscriptions/${var.suscription_id}"
  role_definition_name = azurerm_role_definition.iam_reader.name
  principal_id         = azuread_user.iam_reader.object_id
}
