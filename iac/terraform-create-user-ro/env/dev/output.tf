output "user_principal_name" {
  description = "User principal name"
  value = var.user_principal_name
}

output "password" {
  description = "Password of the user principal"
  value = var.password
}
