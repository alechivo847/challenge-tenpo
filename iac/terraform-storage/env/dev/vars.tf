## Global Variables ##

variable "suscription_id" {
 description = "Suscription ID of Azure"
 type        = string
}

variable "location" {
 type    = string
 default = "East US"
}

variable "source" {
 type    = string
 default = "local-file.zip"
}
