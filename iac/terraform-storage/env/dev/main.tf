terraform {
  backend "azurerm" {
    resource_group_name  = "storage"
    storage_account_name = "storage<RANDOM-STRING>"
    container_name       = "storage"
    key                  = "terraform.storage"
  }
}
terraform {
  required_version = ">=1.0.0"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.63.0"
    }
  }
}
provider "azurerm" {
  version = ">=3.63.0"
  features {}
  suscription_id = var.suscription_id
}
